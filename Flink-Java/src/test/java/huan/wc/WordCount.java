package huan.wc;

import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class WordCount {
    public static void main(String[] args) throws Exception{
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism( 1 );

        String host = "DESKTOP-B32P661";
        int port = 9999;
        DataStreamSource<String> source = env.socketTextStream( host, port );


        source.print();
        env.execute("WordCount");
    }
}
