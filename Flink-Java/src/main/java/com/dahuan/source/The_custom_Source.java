package com.dahuan.source;

import com.dahuan.bean.SensorReading;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;


import java.util.HashMap;
import java.util.Random;

public class The_custom_Source {
    public static void main(String[] args) throws Exception {
        //创建流处理环境
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //设置全局并行度(任务)
        env.setParallelism( 1 );
        //设置事件时间
        env.setStreamTimeCharacteristic( TimeCharacteristic.EventTime );
        //自定义数据源
        DataStreamSource<SensorReading> sensorReadingDataStreamSource = env.addSource( new MySensor() );
        sensorReadingDataStreamSource.print();

        env.execute( "The_custom_Source" );

    }
}

