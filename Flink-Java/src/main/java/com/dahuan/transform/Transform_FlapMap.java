package com.dahuan.transform;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

public class Transform_FlapMap {

    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism( 1 );

        //从文件读取数据
        String path = "E:\\Project\\FlinkTutorials\\Flink-Scala\\src\\main\\resources\\sensor.txt";
        DataStreamSource<String> stringDataStreamSource = env.readTextFile( path );

        //按逗号分隔字段
        stringDataStreamSource.flatMap( new FlatMapFunction<String, String>() {
            @Override
            public void flatMap(String value, Collector<String> out) throws Exception {
                //每个字段的值按逗号分隔
                String[] splits = value.split( "," );
                //使用for循环将splits遍历出来变成split
                for (String split : splits) {
                    //输出
                    out.collect( split );
                }
            }
        } ).print();


        env.execute( "Transform_FlapMap" );
    }

}
