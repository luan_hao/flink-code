package com.dahuan.transform;

import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public class Transform_Partition {
    public static void main(String[] args) throws Exception{
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism( 4 );

        //从文件读取数据
        String path = "E:\\Project\\FlinkTutorials\\Flink-Scala\\src\\main\\resources\\sensor.txt";

        DataStreamSource<String> source = env.readTextFile( path );

        source.print("input");

        //1.shuffle
        DataStream<String> shuffleStream = source.shuffle();
        shuffleStream.print("shuffle");

        env.execute("Transform_Partition");
    }
}
