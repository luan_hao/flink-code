package com.huan.source

import org.apache.flink.streaming.api.scala._

object FileSource {
  def main(args: Array[String]): Unit = {
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    //从文件获取数据
    val path = "E:\\Project\\FlinkTutorials\\Flink-Scala\\src\\main\\resources\\sensor.txt"
    val inputStream = env.readTextFile(path)

    inputStream.print().setParallelism(1)

    env.execute("FileSource")
  }
}
