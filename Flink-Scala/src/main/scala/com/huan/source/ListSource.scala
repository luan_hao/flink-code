package com.huan.source

import com.huan.table.SensorReading
import org.apache.flink.streaming.api.scala._


object ListSource {
  def main(args: Array[String]): Unit = {

    //创建流处理环境
    val env = StreamExecutionEnvironment.getExecutionEnvironment

    val dataList = List(SensorReading("sensor_1", 1547718199, 35.8),
         SensorReading("sensor_6",1547718201,15.4),
         SensorReading("sensor_7",1547718202,6.7),
         SensorReading("sensor_10",1547718205,38.1)
        )

    val dataStream = env.fromCollection(dataList)

    dataStream.print().setParallelism(1)

    env.execute("ListSource")
  }
}
